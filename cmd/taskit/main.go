package main

import (
	"io"
	"os"
	"slovy/taskit/internal/app/core"
	"slovy/taskit/internal/app/task"
	"slovy/taskit/internal/app/task/database"
	fileDB "slovy/taskit/pkg/file_db"
	"slovy/taskit/pkg/log"
)

func main() {
	log.Init()

	logger := log.GetLogger()

	fileDatabase, err := fileDB.New()
	if err != nil {
		logger.Fatal(err)
	}

	repo, err := database.New(fileDatabase)
	if err != nil {
		logger.Fatal(err)
	}
	defer func(repo io.Closer) {
		err = repo.Close()
		if err != nil {
			logger.Fatal(err)
		}
	}(repo)

	service := task.NewService(repo)
	taskApp := core.NewTaskItApp(service, logger)
	taskApp.RunCommand(os.Args)
}
