package log

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"os"
	"path"
	"runtime"
	"time"
)

var dir = "/taskit"
var loggerEntry *logrus.Entry
var Level *levelHolder

type WrapLevel logrus.Level

type levelHolder struct {
	INFO  WrapLevel
	WARN  WrapLevel
	ERROR WrapLevel
	FATAL WrapLevel
	DEBUG WrapLevel
	TRACE WrapLevel
}

type Logger struct {
	*logrus.Entry
}

func GetLogger() *Logger {
	return &Logger{loggerEntry}
}

func Init() {
	if loggerEntry != nil {
		return
	}

	logger := logrus.New()
	logger.SetReportCaller(true)
	logger.SetLevel(logrus.DebugLevel)

	logger.Formatter = &logrus.TextFormatter{
		CallerPrettyfier: func(frame *runtime.Frame) (function string, file string) {
			filename := path.Base(frame.File)
			return fmt.Sprintf("%s()", frame.Function), fmt.Sprintf("%s:%d", filename, frame.Line)
		},
		FullTimestamp: true,
	}

	Level = &levelHolder{
		INFO:  WrapLevel(logrus.InfoLevel),
		WARN:  WrapLevel(logrus.WarnLevel),
		ERROR: WrapLevel(logrus.ErrorLevel),
		FATAL: WrapLevel(logrus.FatalLevel),
		DEBUG: WrapLevel(logrus.DebugLevel),
		TRACE: WrapLevel(logrus.TraceLevel),
	}

	taskitPath := os.Getenv("taskit")
	if taskitPath != "" {
		dir = taskitPath + "/logs"
	}

	err := os.MkdirAll(dir, 0755)
	if err != nil || os.IsExist(err) {
		panic(err)
	}

	filename := fmt.Sprintf(dir+"/taskit-%s.log", time.Now().Format("2006-01-02"))
	logFile, err := os.OpenFile(filename, os.O_CREATE|os.O_WRONLY|os.O_APPEND, 0660)
	if err != nil {
		panic(err)
	}

	logger.SetOutput(logFile)
	loggerEntry = logrus.NewEntry(logger)
}
