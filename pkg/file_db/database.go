package file_db

import (
	"encoding/json"
	"os"
)

var dbFilename = "/taskit/task.db"
var sequenceFilename = "/taskit/sequence.db"

const filePermissions = 0744

type FileDB struct {
	dbFile       *os.File
	sequenceFile *os.File
}

func New() (*FileDB, error) {
	taskitPath := os.Getenv("taskit")
	if taskitPath != "" {
		dbFilename = taskitPath + "/" + "task.db"
		sequenceFilename = taskitPath + "/" + "sequence.db"
	}

	dbFile, err := os.OpenFile(dbFilename, os.O_RDWR|os.O_CREATE|os.O_APPEND, filePermissions)
	if err != nil {
		return nil, err
	}

	sequenceFile, err := os.OpenFile(sequenceFilename, os.O_RDWR|os.O_CREATE|os.O_APPEND, filePermissions)
	if err != nil {
		return nil, err
	}

	return &FileDB{dbFile: dbFile, sequenceFile: sequenceFile}, nil
}

func (f *FileDB) Close() error {
	err := f.dbFile.Close()
	if err != nil {
		return err
	}

	err = f.sequenceFile.Close()
	return err
}

func (f *FileDB) GetAll() ([]byte, error) {
	rawBytes, err := os.ReadFile(f.dbFile.Name())
	if err != nil {
		return nil, err
	}

	return rawBytes, nil
}

func (f *FileDB) SaveTasks(data []byte) error {
	return writeBytes(data, f.dbFile)
}

func (f *FileDB) GetCurrentID() (int64, error) {
	rawBytes, err := os.ReadFile(f.sequenceFile.Name())
	if err != nil {
		return 0, err
	}

	var curId int64 = 1
	if len(rawBytes) == 0 {
		return curId, err
	}

	err = json.Unmarshal(rawBytes, &curId)
	if err != nil {
		return 0, err
	}

	return curId, err
}

func (f *FileDB) SaveID(id int64) error {
	rawBytes, err := json.Marshal(id)
	if err != nil {
		return err
	}
	return writeBytes(rawBytes, f.sequenceFile)
}

func writeBytes(data []byte, file *os.File) error {
	err := os.WriteFile(file.Name(), data, filePermissions)
	return err
}
