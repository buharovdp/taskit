## TaskIt

### What is it?

TaskIt — CLI task list with colorized interface

### Why?

Because lately I've been liking console utilities.

### Build and start using

#### Windows

```powershell
# build and move convenient dir
go build cmd/taskit/main.go
rename main.exe taskit.exe
move taskit.exe D:\YourDirectory\taskit\.

# SET env variables
SET taskit=D:\YourDirectory\taskit\
SET PATH=%PATH%;D:\YourDirectory\taskit\
```

#### Unix
```bash
go build cmd/taskit/main.go
mv main /YourDirectory/taskit/taskit
echo "export taskit=/YourDirectory/taskit/" >> ~/.bashrc
```

### Commands

Get help:\
`taskit help`

Create task:\
`taskit add "Do something"`

Complete task:\
`taskit done <TASK_ID>`

Delete tasks:\
`taskit remove <TASK_ID>`

Get all tasks:\
`taskit ls`

Get active tasks:\
`taskit ls -a`

Get expired tasks:\
`taskit ls -e`

### Screenshots

![list.png](./img/1.png)

### Stack

Golang + Logrus for logging
