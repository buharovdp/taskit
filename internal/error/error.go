package app_error

import (
	"encoding/json"
	"errors"
)

type AppError struct {
	Err     error
	Message string
}

// NewAppError returns new AppError with custom message if default errors is not enough
func NewAppError(message string) *AppError {
	return &AppError{
		Err:     errors.New(message),
		Message: message,
	}
}

// Error needed to implement Error interface
func (e *AppError) Error() string {
	return e.Err.Error()
}

// Unwrap needed to easy unwrap error
func (e *AppError) Unwrap() error {
	return e.Err
}

// Marshal serialize error into []byte
func (e *AppError) Marshal() ([]byte, error) {
	bytes, err := json.Marshal(e)
	if err != nil {
		return nil, err
	}

	return bytes, nil
}

var (
	TaskNotFound    = NewAppError("such task not found")
	CommandNotFound = NewAppError("such command not found, use <taskit help> to get more info")
	InvalidID       = NewAppError("can't convert ID to INT")
	NotEnoughArgs   = NewAppError("length of arguments is invalid, use <taskit help> to get more info")
	InvalidKey      = NewAppError("unknown key, use <taskit help> to get more info")
)
