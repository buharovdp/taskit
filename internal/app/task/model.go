package task

import (
	"fmt"
	"time"
)

type ID int64
type Status string

const (
	active    Status = "active"
	completed Status = "completed"
	deleted   Status = "deleted"
	expired   Status = "expired"
)

type statusHolder struct {
	ACTIVE    Status
	COMPLETED Status
	DELETED   Status
	EXPIRED   Status
}

var Statuses = &statusHolder{
	ACTIVE:    active,
	COMPLETED: completed,
	DELETED:   deleted,
	EXPIRED:   expired,
}

type Task struct {
	ID         `json:"id"`
	Title      string    `json:"title"`
	Deadline   time.Time `json:"deadline"`
	CompleteAt time.Time `json:"complete_at"`
	Status     `json:"status"`
}

func (id ID) String() string {
	return fmt.Sprintf("%d", id)
}

func NewTaskFromDTO(id ID, dto *CreateDTO) *Task {
	today := time.Now().Round(0)
	yyyy, mm, dd := today.Date()

	return &Task{
		ID:       id,
		Title:    dto.Title,
		Deadline: time.Date(yyyy, mm, dd+1, 0, 0, 0, 0, today.Location()),
		Status:   Statuses.ACTIVE,
	}
}
