package task

type Repository interface {
	Close() error
	Get(id ID) (*Task, error)
	GetAll() []*Task
	Save(task *Task)
	GetNextID() ID
	ChangeStatus(id ID, status Status) error
}
