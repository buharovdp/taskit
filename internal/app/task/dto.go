package task

import "time"

type CreateDTO struct {
	Title string
}

type RequestDTO struct {
	ID
	Title      string
	CompleteAt time.Time
	Status
}
