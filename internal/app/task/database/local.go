package database

import (
	"encoding/json"
	"fmt"
	"slovy/taskit/internal/app/task"
	appError "slovy/taskit/internal/error"
	fileDB "slovy/taskit/pkg/file_db"
	"sync"
	"time"
)

type storage struct {
	db    *fileDB.FileDB
	data  map[task.ID]*task.Task
	curID int64
	mu    *sync.RWMutex
}

func New(db *fileDB.FileDB) (*storage, error) {
	// read tasks
	rawBytes, err := db.GetAll()
	if err != nil {
		return nil, fmt.Errorf("error getting tasks from database: %v", err)
	}

	var data map[task.ID]*task.Task
	if len(rawBytes) == 0 {
		data = make(map[task.ID]*task.Task)
	} else {
		err = json.Unmarshal(rawBytes, &data)
		if err != nil {
			return nil, fmt.Errorf("error to unmarshal tasks: %v", err)
		}
	}

	// read last id
	currId, err := db.GetCurrentID()
	if err != nil {
		return nil, fmt.Errorf("error getting id from database: %v", err)
	}

	return &storage{
		db:    db,
		data:  data,
		curID: currId,
		mu:    new(sync.RWMutex),
	}, nil
}

func (s *storage) Close() error {
	// save tasks
	s.mu.RLock()
	rawBytes, err := json.Marshal(s.data)
	s.mu.RUnlock()
	if err != nil {
		return err
	}

	err = s.db.SaveTasks(rawBytes)
	if err != nil {
		return err
	}

	// save last id
	err = s.db.SaveID(s.curID)
	if err != nil {
		return err
	}

	return s.db.Close()
}

func (s *storage) Get(id task.ID) (*task.Task, error) {
	s.mu.RLock()
	defer s.mu.RUnlock()
	v, ok := s.data[id]
	if !ok {
		return nil, appError.TaskNotFound
	}
	return v, nil
}

func (s *storage) GetAll() []*task.Task {
	list := make([]*task.Task, 0, len(s.data))
	for _, t := range s.data {
		s.mu.RLock()
		list = append(list, t)
		s.mu.RUnlock()
	}
	return list
}

func (s *storage) Save(task *task.Task) {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.data[task.ID] = task
}

func (s *storage) GetNextID() task.ID {
	s.mu.Lock()
	defer s.mu.Unlock()
	s.curID++
	return task.ID(s.curID)
}

func (s *storage) ChangeStatus(id task.ID, status task.Status) error {
	s.mu.Lock()
	defer s.mu.Unlock()
	if _, ok := s.data[id]; !ok {
		return appError.TaskNotFound
	}
	if s.data[id].Status == task.Statuses.DELETED {
		return appError.TaskNotFound
	}
	s.data[id].Status = status
	if status == task.Statuses.COMPLETED {
		s.data[id].CompleteAt = time.Now()
	}
	return nil
}
