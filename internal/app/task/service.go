package task

import (
	"time"
)

type Service interface {
	GetActive() []*Task
	GetExpired() []*Task
	GetAll() []*Task
	Add(newTask *CreateDTO)
	Complete(id ID) error
	Delete(id ID) error
}

type service struct {
	repo Repository
}

func NewService(repo Repository) Service {
	return &service{repo}
}

func (s *service) GetActive() []*Task {
	allTasks := s.repo.GetAll()
	activeTasks := make([]*Task, 0, len(allTasks))
	now := time.Now()
	for _, t := range allTasks {
		if t.Status == Statuses.ACTIVE && t.Deadline.After(now) {
			activeTasks = append(activeTasks, t)
		}
	}
	return activeTasks
}

func (s *service) GetExpired() []*Task {
	allTasks := s.repo.GetAll()
	expiresTasks := make([]*Task, 0, len(allTasks))
	now := time.Now()
	for _, t := range allTasks {
		if t.Status == Statuses.DELETED {
			continue
		}

		if t.Status == Statuses.EXPIRED || (t.Deadline.Before(now) && t.Status == active) {
			expiresTasks = append(expiresTasks, t)
		}
	}
	return expiresTasks
}

func (s *service) GetAll() []*Task {
	data := s.repo.GetAll()
	for _, t := range data {
		if t.Status == active && t.Deadline.Before(time.Now()) {
			t.Status = Statuses.EXPIRED
		}
	}
	return data
}

func (s *service) Add(newTask *CreateDTO) {
	task := NewTaskFromDTO(s.repo.GetNextID(), newTask)
	s.repo.Save(task)
}

func (s *service) Complete(id ID) error {
	return s.repo.ChangeStatus(id, Statuses.COMPLETED)
}

func (s *service) Delete(id ID) error {
	return s.repo.ChangeStatus(id, Statuses.DELETED)
}
