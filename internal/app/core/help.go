package core

import "strings"

func (app *TaskItApp) Help() string {
	var sb strings.Builder
	sb.WriteString("TaskIt — CLI task list with colorized interface.\nCommands:\n")
	for k, cmd := range commands {
		sb.WriteString("\t" + k + ":\n\t\t" + cmd + "\n")
	}

	s := sb.String()
	return s[:len(s)-1]
}
