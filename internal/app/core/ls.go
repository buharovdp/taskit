package core

import (
	"fmt"
	"slovy/taskit/internal/app/task"
	appError "slovy/taskit/internal/error"
	"sort"
	"strings"
)

func (app *TaskItApp) Ls(args []string) (string, error) {
	var res []*task.Task
	if len(args) == 2 {
		res = app.service.GetAll()
	} else if len(args) == 3 && args[2] == "-e" {
		res = app.service.GetExpired()
	} else if len(args) == 3 && args[2] == "-a" {
		res = app.service.GetActive()
	} else {
		return "", appError.InvalidKey
	}

	sort.Slice(res[:], func(i, j int) bool {
		return res[i].ID < res[j].ID
	})

	var sb strings.Builder
	sb.WriteString("+------+------------------+-----------+--------------+\n")
	sb.WriteString("| ID   | title            | status    | completed at |\n")
	sb.WriteString("+------+------------------+-----------+--------------+\n")
	for _, t := range res {
		var line string
		switch t.Status {
		case task.Statuses.ACTIVE:
			line = fmt.Sprintf("| %s | %s | %s%s%s |       —      |\n", addSpaces(t.ID, 4), addSpaces(t.Title, 16), blue, addSpaces(t.Status, 9), reset)
		case task.Statuses.EXPIRED:
			line = fmt.Sprintf("| %s | %s | %s%s%s |       —      |\n", addSpaces(t.ID, 4), addSpaces(t.Title, 16), red, addSpaces(t.Status, 9), reset)
		case task.Statuses.COMPLETED:
			line = fmt.Sprintf("| %s | %s | %s%s%s | %s |\n", addSpaces(t.ID, 4), addSpaces(t.Title, 16), green, addSpaces(t.Status, 9), reset, t.CompleteAt.Format("02.01, 03:04"))
		}
		sb.WriteString(line)
	}
	sb.WriteString("+------+------------------+-----------+--------------+")
	return sb.String(), nil
}

func addSpaces(o any, length int) string {
	s := fmt.Sprintf("%v", o)
	if len(s) <= length {
		return s + strings.Repeat(" ", length-len(s))
	} else {
		return s[:length-3] + "..."
	}
}
