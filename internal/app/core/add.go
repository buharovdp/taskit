package core

import (
	"slovy/taskit/internal/app/task"
	appError "slovy/taskit/internal/error"
	"strings"
)

func (app *TaskItApp) Add(args []string) (string, error) {
	if len(args) != 3 {
		return "", appError.NotEnoughArgs
	}

	taskTitle := strings.Replace(args[2], `"`, ``, -1)
	app.service.Add(&task.CreateDTO{Title: taskTitle})
	return "task was added", nil
}
