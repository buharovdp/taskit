package core

import (
	"fmt"
	"slovy/taskit/internal/app/task"
	appError "slovy/taskit/internal/error"
	"slovy/taskit/pkg/log"
	"strconv"
)

var reset = "\033[0m"
var red = "\033[31m"
var yellow = "\033[33m"
var blue = "\033[36m"
var green = "\033[32m"

var commands = map[string]string{
	`help`:   `get list of commands`,
	`add`:    `add a task: taskit add "name of task"`,
	`done`:   `complete task: taskit complete <TASK_ID>`,
	`remove`: `removing task: taskit remove <TASK_ID>`,
	`ls`:     `getting all task: taskit ls`,
	`ls -a`:  `getting all active task: taskit ls -a`,
	`ls -e`:  `getting all expired task: taskit ls -e`,
}

type Service interface {
	GetActive() []*task.Task
	GetExpired() []*task.Task
	GetAll() []*task.Task
	Add(newTask *task.CreateDTO)
	Complete(id task.ID) error
	Delete(id task.ID) error
}

type TaskItApp struct {
	service Service
	log     *log.Logger
}

func NewTaskItApp(service Service, log *log.Logger) *TaskItApp {
	return &TaskItApp{service: service, log: log}
}

func (app *TaskItApp) RunCommand(executeArgs []string) {
	res, err := app.executeCommand(executeArgs)
	if err != nil {
		fmt.Println(red + "Error: " + reset + err.Error())
	} else {
		fmt.Println(res + reset)
	}
}

func (app *TaskItApp) executeCommand(args []string) (string, error) {
	command := args[1]

	if _, ok := commands[command]; !ok {
		return "", appError.CommandNotFound
	}

	switch command {
	case "help":
		return app.Help(), nil
	case "remove":
		return app.Remove(args)
	case "done":
		return app.Done(args)
	case "add":
		return app.Add(args)
	case "ls":
		return app.Ls(args)
	default:
		return "", appError.CommandNotFound
	}
}

func getID(strID string) (task.ID, error) {
	id, err := strconv.ParseInt(strID, 10, 64)
	if err != nil {
		return task.ID(0), appError.InvalidID
	}
	return task.ID(id), nil
}
