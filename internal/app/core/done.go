package core

import (
	appError "slovy/taskit/internal/error"
)

func (app *TaskItApp) Done(args []string) (string, error) {
	if len(args) != 3 {
		return "", appError.NotEnoughArgs
	}

	id, err := getID(args[2])
	if err != nil {
		return "", err
	}

	return "task was done", app.service.Complete(id)
}
